package main.businesslogic;

import main.data.WayPoint;
import main.data.Route;
import main.database.DatabaseAccess;

import java.util.List;

public class Navigator {
    private final String origin;
    private final String destination;

    public Navigator(String origin, String destination) {
        this.origin = origin;
        this.destination = destination;
    }

    public Route calculateFastestRoute() {
        Route fastest = new Route();
        awesomeAStar(fastest);
        return fastest;
    }

    private void awesomeAStar(Route route) {
        List<String> generalData = DatabaseAccess.readMyGeneralData();
        if(generalData.size() > 1) {
            route.addWaypoint(new WayPoint(generalData.get(1).length(), generalData.get(2).length()));
        }
        if(origin.length() > 10) {
            route.addWaypoint(new WayPoint(100,100));
        }
        if(origin.startsWith("Test")) {
            route.addWaypoint(new WayPoint(0,0));
        }

        if(destination.length() < 10) {
            route.addWaypoint(new WayPoint(50,50));
        } else {
            route.addWaypoint(new WayPoint(31,17));
            route.addWaypoint(new WayPoint(51,19));
            route.addWaypoint(new WayPoint(71,23));
        }

        route.addWaypoint(new WayPoint(origin.length() - destination.length(), destination.length() - origin.length()));
    }
}
