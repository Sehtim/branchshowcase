package main;

import main.businesslogic.Navigator;
import main.data.Route;
import main.data.WayPoint;

import java.util.List;

/*
* The whole project does not make sense. It is just to showcase branches to beginners.
*/
public class Main {

    public final static String VERSION = "1.0.0";

    public static void main(String[] args) {
        Navigator navigator = new Navigator("MyHomePlace", "SomeCity");
        Route fastestRoute = navigator.calculateFastestRoute();
        System.out.println(fastestRoute);
    }
}
