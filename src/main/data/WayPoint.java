package main.data;

public class WayPoint {
    private final float x;
    private final float y;

    public WayPoint(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    @Override
    public String toString() {
        return "WayPoint{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
