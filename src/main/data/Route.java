package main.data;

import java.util.ArrayList;
import java.util.List;

public class Route {
    private final List<WayPoint> waypoints;

    public Route() {
        waypoints = new ArrayList<WayPoint>();
    }

    public void setRoute(List<WayPoint> waypoints) {
        this.waypoints.clear();
        this.waypoints.addAll(waypoints);
    }

    public void addWaypoint(WayPoint nextPoint) {
        waypoints.add(nextPoint);
    }

    public List<WayPoint> getWaypoints() {
        return new ArrayList<WayPoint>(waypoints);
    }

    @Override
    public String toString() {
        return "Route{" +
                "waypoints=" + waypoints +
                '}';
    }
}
